let $ = require("jquery");
let fs = require("fs");
let remote = require("electron").remote,
  dialog = remote.dialog;

$("#openFile").on("click", function() {
  dialog.showOpenDialog(
    {
      filters: [
        { name: "text", extensions: ["txt"] },
        { name: "JS", extensions: ["js"] }
      ]
    },
    function(filenames) {
      if (!filenames) return;

      let filename = filenames[0];

      fs.readFile(filename, "utf-8", function(err, content) {
        let noWhiteSpace = content.replace(/\s/g, "");

        let fileDataArray = noWhiteSpace.split("require");

        GetModules(fileDataArray);

        $("#counter").text(content);
      });
    }
  );
});

function GetModules(fileDataArray) {
  for (let i = 0; i < fileDataArray.length; i++) {
    if (
      fileDataArray[i].startsWith('("') ||
      fileDataArray[i].startsWith("('")
    ) {
      let doubleQuotes = fileDataArray[i].indexOf('")');
      let singleQuotes = fileDataArray[i].indexOf("')");
      let lastIndex = doubleQuotes > 0 ? doubleQuotes : singleQuotes;
      let getModule = fileDataArray[i].slice(2, lastIndex);
      console.log(getModule);
    }
  }
}
